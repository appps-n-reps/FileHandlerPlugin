package at.shaggy.cordova.plugin;

import android.app.Activity;

import android.content.Intent;
import android.content.ClipData;

import android.net.Uri;


import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;


import org.json.*;


/**
 * This class echoes a string called from JavaScript.
 */
public class FileHandler extends CordovaPlugin
{
  private int num_ = 0;
  private CallbackContext cbctx_ = null;
  private static final int CHOOSE_REQUEST = 1;
  private JSONArray args = null;

  @Override
  public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException
  {
    this.cbctx_ = callbackContext;

    JSONObject js_args = args.getJSONObject(0);
    switch(action)
    {
      case "choose":
        this.choose(js_args);
        return true;
      default:
        this.cbctx_.error("Wrong method name.");
        return false;
    }
  }

  private void choose(JSONObject js_args) throws JSONException
  {
    JSONObject options = js_args.getJSONObject("options");
    boolean persistent = true;
    String title = "";
    String[] types = null;

    try
    {
      title = options.getString("title");
      this.num_ = options.getInt("num");
      persistent = options.getBoolean("persistent");
      JSONArray js_types = options.getJSONArray("types");
      types = new String[js_types.length()];
      for(int i = 0; i < js_types.length(); i++)
      {
        types[i] = js_types.getString(i);
      }
    }
    catch(Exception e)
    {
      this.cbctx_.error(e.getMessage());
    }

    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    intent.addCategory(Intent.CATEGORY_OPENABLE);

    if(types.length == 1)
      intent.setType(types[0]);
    else
      intent.putExtra(Intent.EXTRA_MIME_TYPES, types);

    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);

    intent.putExtra(Intent.EXTRA_TITLE, title);

    if(this.num_ != 1)
      intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

    Intent chooser = Intent.createChooser(intent, title);
    cordova.startActivityForResult(this, chooser, CHOOSE_REQUEST);

  }
  /*
    @Override
    public void onRequestPermissionResult(int requestCode, String[] permissions,
                                          int[] grantResults) throws JSONException
    {
      for(int r:grantResults)
      {
        if(r == PackageManager.PERMISSION_DENIED)
        {
          this.cbctx_.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, PERMISSION_DENIED_ERROR));
          return;
        }
      }

      switch(requestCode)
      {
        case SEARCH_REQ_CODE:
          search(executeArgs);
          break;
        case SAVE_REQ_CODE:
          save(executeArgs);
          break;
        case REMOVE_REQ_CODE:
          remove(executeArgs);
          break;
      }
    }
  */
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    if(resultCode != Activity.RESULT_OK)
    {
      switch (resultCode)
      {
        case Activity.RESULT_CANCELED:
          this.cbctx_.error("User canceled activity.");
          break;
        default:
          this.cbctx_.error("Not handled result code: " + resultCode);
          break;
      }
      return;
    }

    switch(requestCode)
    {
      case CHOOSE_REQUEST:
        handleChosenFiles(data);
        break;
    }
  }

  private void handleChosenFiles(Intent data)
  {
    JSONObject ret_obj = new JSONObject();
    JSONArray uris = new JSONArray();

    try
    {
      Uri uri = data.getData();
      if(uri != null)
        uris.put(uri);
      else
      {
        ClipData clipdata = data.getClipData();
        for(int i = 0; i < clipdata.getItemCount(); ++i)
          uris.put(clipdata.getItemAt(i).getUri());
      }
      ret_obj.put("uris", uris);
      this.cbctx_.success(ret_obj);
    }
    catch(Exception e)
    {
      this.cbctx_.error(e.getMessage());
    }
  }
}