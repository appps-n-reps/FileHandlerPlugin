/*
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
*/


let cordova = require('cordova');

function FileHandler()
{
  this.choose = function(successCallback, errorCallBack, user_options)
  {
    let args = {};
    let options  =
        {
          types : user_options.types ? user_options.types : ["*/*"],
          num   : user_options.num > 0 ? user_options.num : 0, // 0 (unlimited) and 1 supported
          persistent: user_options.persistent ? user_options.persistent : false, //Not yet supported
          title : user_options.title ? user_options.title : "Choose files"
        };

    args.options = options;
    cordova.exec(successCallback, errorCallBack, "FileHandler", "choose", [args]);
  }
}

module.exports = new FileHandler();

