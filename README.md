# Cordova Filehandler Plugin
The plugin shall make it easy for developers to handle files (e.g. let the user
select/delete files, copy files).

This is a project done in spare time. We hope to extend it as much (and as fast)
as possible, as we also have to study and work.



## Install with Cordova CLI

```bash
cordova plugin add https://gitlab.com/appps-n-reps/FileHandlerPlugin.git
```
## API

shaggy.filehandler returns a new FileHandler object.
The object currently (2018.07.21) has the following methods:

```javascript
choose(successCallback, failureCallback, options);
```

## Examples
### choose

```javascript
let filehandler = shaggy.filehandler;
function success(files)
{
  console.log(files);
  let uris = files.uris; //array of selected uris   
   //You can now work with the uris, to access the selected files.
  for(uri in uris)
  {
     window.resolveLocalFileSystemURL(uri,
       function a(f){...},
       function b(e){...});
  }
}

function error(message)
{
  console.log("Got the following error: " + error);
}

let options  =
{
  types : ["*/*"], //array of strings
  num   : 0 // 0 (unlimited) and 1 supported
  persistent: false, //Not yet supported
  title : "Choose files(s)"
};
filehandler.choose(success, error, options);
```
